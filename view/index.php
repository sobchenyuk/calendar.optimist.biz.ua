
<?php include PATCH . '/view/section/header.php' ?>

<div class="wrapper">

    <section class="wrapper-top">
        <div class="container">

            <h3 class="center-align">Календарь событий</h3>

            <div class="row">
                <div class="col s8 fast-add__box">
                    <div class="row">
                        <div class="col s8 fast-add">
                            <a id="modal1" class="waves-effect waves-light btn blue">Быстрое
                                добовление</a>
                        </div>
                    </div>

                    <div class="__modal modal1" style="z-index: 1003;">
                        <form id="fast-add__form" class="fast-add__form" action="#">
                            <div class="modal-content">
                                <h5>Добавить событие</h5>
                                <input type="hidden" name="slug" class="slug value">
                                <div class="row">
                                    <div class="input-field col s12 valid">
                                        <input id="datepicker"
                                               name="datepicker"
                                               type="text" class="datepicker inp" autocomplete="off">
                                        <label class="active"
                                               for="datepicker">Укажите дату</label>
                                    </div>
                                    <div class="input-field col s12 valid">
                                        <input id="event"
                                               name="event"
                                               type="text" class="event inp value" autocomplete="off">
                                        <label class="active"
                                               for="event">Событие</label>
                                    </div>
                                    <p class="checkbox-field">
                                        <input type="checkbox" id="pd"/>
                                        <label for="pd">Описание события</label>
                                    </p>
                                    <div class="additional-field" style="display: none">
                                        <div class="input-field col s12">
                                            <textarea id="description"
                                                      name="description"
                                                      class="materialize-textarea inp value"></textarea>
                                            <label class="active"
                                                   for="description">Описание...</label>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="submit" class="__modal-close
                                waves-effect
                                btn-flat
                                waves-light
                                fast-add__create
                                blue-grey lighten-5">
                                    Создать
                                </button>
                            </div>
                            <div class="close__btn">
                                <button type="button" class="close">
                                    <span>×</span>
                                </button>
                            </div>
                        </form>
                    </div>

                </div>
                <div class="col s4 wrapper-top__search">

                    <div class="row">
                        <form class="col s12">
                            <div class="row wrapper-top__form">
                                <div class="input-field col s8 top-form__field">
                                    <input value="" id="search" type="text" class="validate" autocomplete="off">
                                    <label class="active" for="search">Быстрый поиск события</label>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

        </div>
    </section>

    <section class="wrapper-control">
        <div class="container">

            <div class="row">
                <div class="col s6">
                    <div id="control-date" class="row">

                        <div class="col s1">
                            <button id="control-left" class="waves-effect waves-light btn blue-grey lighten-4">
                                <i class="tiny material-icons">chevron_left</i>
                            </button>
                        </div>

                        <div class="col s5 wrapper-s-w">
                            <div id="control-select" class="row">
                                <div class="input-field col s6">
                                    <select id="monthNames" class="browser-default blue-grey lighten-5"></select>
                                    <label></label>
                                </div>
                                <div class="input-field col s6">
                                    <select id="years" class="browser-default blue-grey lighten-5"></select>
                                    <label></label>
                                </div>
                            </div>
                        </div>

                        <div class="col s1">
                            <button id="control-right" class="waves-effect waves-light btn blue-grey lighten-4">
                                <i class="tiny material-icons">chevron_right</i>
                            </button>
                        </div>

                        <div class="col s2">
                            <button id="control-now" class="waves-effect waves-light btn blue-grey lighten-4">
                                Сегодня
                            </button>
                        </div>

                    </div>
                </div>

                <div class="col s6 video">
                    <div class="row">
                        <div class="col s6 video">
                            <div class="video-pulse">
                                <div class="video-pulse__container">
                                    <p class="video-pulse__container--title">Как пользоваться календарем ?</p>
                                    <a class="btn btn-floating pulse blue-grey lighten-4 data-fancybox"
                                       href="https://www.youtube.com/watch?v=_sI_Ps7JSEk" data-rel="media">
                                        <i class="material-icons">live_tv</i>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col s3 video">
                            <div class="share center-align">
                                <small><b>ПОДЕЛИТЬСЯ</b></small>
                                <div class="share-links"></div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </section>

    <section class="wrapper-tasks">

        <ul id="c-click" class="collapsible left-align" data-collapsible="expandable">
            <li>
                <div class="collapsible-header center-align">
                    <h5>
                        Осталось <br class="cl"> доделать
                    </h5>
                </div>
                <div class="collapsible-body">
                    <span class="left-align">
                        <ul class="wrapper-tasks__list">
                            <li>Работа с базой</li>
                            <li>Настроить поиск</li>
                            <li>Создание кабинета для пользователя</li>
                            <li>Необходимо <b>10000</b> грн</li>
                        </ul>
                    </span>
                </div>
            </li>
        </ul>

        <ul id="w-c-click" class="collection">
            <li class="collection-item" style="display: flex;justify-content: space-between;align-items: center;">
                <span class="center-align"> <b>
                     Поддержать <br>
                100 грн.
                    </b>
                </span>
                <form id="payment" name="payment" class="center-align payment"
                      method="post" action="https://sci.interkassa.com/" enctype="utf-8">
                    <input type="hidden" name="ik_co_id" value="5a81dfb33d1eaf01708b4567" />
                    <input type="hidden" name="ik_pm_no" value="ID_4233" />
                    <input type="hidden" name="ik_am" value="100" />
                    <input type="hidden" name="ik_cur" value="UAH" />
                    <input type="hidden" name="ik_desc" value="Развитие проекта" />
                    <button type="submit" class="btn-floating waves-effect waves-light red"  value="Потдержать проект">
                        <i class="fas fa-dollar-sign"></i>
                    </button>
                </form>
            </li>
            <li class="collection-item center-align">
                <a id="modal2o" href="#modal2"
                   class="btn waves-effect waves-light modal-trigger yellow darken-1">
                    Написать
                </a>
            </li>
        </ul>

    </section>

    <section id="calendar">

        <div class="container">

            <div id="grid" class="grid__list"></div>

        </div>
    </section>

</div>


<footer id="footer" class="page-footer hidden grey grey darken-1">
    <div class="container">
        <div class="row">
            <div class="col l6 s12">
                <h5 class="white-text">Календарь событий</h5>
                <p class="grey-text text-lighten-4">
                    Автор проекта:
                    <a href="https://www.facebook.com/andreysobchenyuk" class="link" target="_blank">
                        <b><i>Собченюк Андрей Владимирович</i></b>
                    </a>
                </p>
            </div>
            <div class="col l4 offset-l2 s12">
                <h5 class="white-text">Контакты</h5>
                <ul>
                    <li>
                        <a class="grey-text text-lighten-3" href="#!">
                            Mтс: +380(66)9353050
                        </a>
                    </li>
                    <li>
                        <a class="grey-text text-lighten-3" href="#!">
                            Telegram: +380(66)9353050
                        </a>
                    </li>
                    <li>
                        <a class="grey-text text-lighten-3" href="#!">
                            Viber: +380(66)9353050
                        </a>
                    </li>
                    <li>
                        <a class="grey-text text-lighten-3" href="#!">
                            Skype: sobchenyuk
                        </a>
                    </li>
                    <li>
                        <a class="grey-text text-lighten-3" href="#!">
                            optimist.biz.ua@gmail.com
                        </a>
                    </li>
                    <li>
                        <a class="grey-text text-lighten-3" href="#!">
                            //vk.com/optimistinua
                        </a>
                    </li>
                    <li>
                        <a class="grey-text text-lighten-3" href="#!">
                            //www.facebook.com/optimist.biz.ua/
                        </a>
                    </li>
                </ul>
            </div>
        </div>

    </div>
    <div class="footer-copyright">
        <div class="container">
            © 2013 - <span id="date">2018</span> <span id="url">Copyright Text</span>
            <a class="grey-text text-lighten-4 right" href="http://optimist.biz.ua/" target="_blank">
                Сайт разработчика
            </a>
        </div>
    </div>
</footer>


<div id="dialog" class="__modal open">
    <form id="form__Now" action="#">
        <div class="modal-content">
            <h5>Добавить событие</h5>
            <h5 class="edit-form__title">Изминить событие</h5>
            <input type="hidden" name="slug" class="slug value">
            <div class="row">
                <div class="input-field col s12 valid">
                    <input id="dateNow" name="datepicker" type="text"
                           class="datepicker inp hasDatepicker" autocomplete="off">
                    <label class="" for="dateNow"></label>
                </div>
                <div class="input-field col s12 valid">
                    <input id="eventNow" name="event" type="text"
                           class="event inp value" autocomplete="off">
                    <label class="" for="eventNow">Событие</label>
                </div>

                <div class="input-field col s12 valid">
                                    <textarea id="descriptionNow" name="description"
                                              class="materialize-textarea inp value"></textarea>
                    <label class="" for="descriptionNow">Описание...</label>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="submit" class="__modal-close
                                waves-effect
                                btn-flat
                                waves-light
                                fast-add__create
                                blue-grey lighten-5">
                <span class="create-btn">
                  Создать
                </span>
                <span class="edit-btn">
                    Изминить
                </span>
            </button>

            <button id="delete" type="button" class="__modal-close
            delete-btn
                                waves-effect
                                btn-flat
                                waves-light
                                fast-add__create
                                blue-grey lighten-5">
                Удалить
            </button>
        </div>
        <div class="close__btn">
            <button type="button" class="close">
                <span>×</span>
            </button>
        </div>
    </form>
</div>

<div id="modal2" class="modal">
    <div class="modal-content">
        <br>
        <form class="submit-form" method="post" enctype="utf-8">
            <div class="modal-content">
                <h4>Форма связи</h4>
                <input type="hidden" name="title"
                       class="value" value="Форма связи">
                <div class="row">
                    <div class="input-field col s12 valid">
                        <input id="name" name="name" type="text"
                               class="inp value" autocomplete="off">
                        <label class="" for="name">Имя</label>
                    </div>
                    <div class="input-field col s12 valid">
                        <input id="phone" name="phone" type="text"
                               class="inp value" autocomplete="off" placeholder="Телефон">
                        <label for="phone"></label>
                    </div>
                    <div class="input-field col s12 valid">
                        <input id="email" name="email" type="text"
                               class="inp value" autocomplete="off">
                        <label class="" for="email">Почта</label>
                    </div>
                    <div class="input-field col s12 valid">
                        <input id="descriptiondes" name="description" type="text"
                               class="inp value" autocomplete="off">
                        <label class="" for="descriptiondes">Текст сообщения</label>
                    </div>
                    <p class="checkbox-field valid">
                        <input type="checkbox" id="checkbox" checked>
                        <label for="checkbox">Я согласен
                            на обработку персональных данных</label>
                    </p>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="__modal-close
                                waves-effect
                                btn-flat
                                waves-light
                                fast-add__create
                                blue-grey lighten-5">
                    Отправить
                </button>
            </div>
        </form>
    </div>
</div>

<?php include PATCH . '/view/section/footer.php' ?>
