

<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

<script src="https://code.jquery.com/jquery-3.2.1.min.js"
        integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
        crossorigin="anonymous"></script>

<script src="/front/assets/js/init.js"></script>

<a href="#" id="toTop" style="display: none;"><span id="toTopHover" style="opacity: 0;"></span>To Top</a>


</body>
</html>

<link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
<script src="/front/assets/js/share/share.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>

<link href="/front/assets/js/jquery-ui/jquery-ui.css" rel="stylesheet">
<script src="/front/assets/js/jquery-ui/jquery-ui.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.2.5/jquery.fancybox.css"/>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.2.5/jquery.fancybox.js"></script>

<script type="text/javascript" src="/front/assets/js/inputmask/jquery.inputmask.bundle.js" charset="utf-8"></script>

<script src="/front/assets/js/nicescroll/jquery.nicescroll.min.js"></script>

<script src="/front/assets/js/pluginsActivater/activator.js"></script>