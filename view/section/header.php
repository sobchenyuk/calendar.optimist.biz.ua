<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="UTF-8">

    <title>Календарь событий</title>

    <meta name="description" content="Профессиональное создание лэндинг пейдж, сайта компании,
        интернет магазина Верстка: landing page, шаблоны лэндингов +380669353050"/>
    <meta name="keywords" content="создание сайтов, создавать сайт, создание партнерского сайта,
         сайт разработка, создание сайта с нуля "/>
    <meta name="generator" content="optimist.biz.ua">
    <meta name="twitter:card" content="summary"/>
    <meta name="twitter:site" content="@sobchenyuk"/>
    <meta name="twitter:title" content="Создание сайта с `0` | Wordpress | Joomla | Landing | Laravel5"/>
    <meta name="twitter:description" content="Профессиональное создание лэндинг пейдж, сайта компании,
        интернет магазина Верстка: landing page, шаблоны лэндингов +380669353050">
    <meta name="twitter:image" content="//optimist.biz.ua/img/site.png">
    <meta property="og:site_name" content="Создание сайта с `0` | Wordpress | Joomla | Landing | Laravel5">
    <meta property="og:url" content="//optimist.biz.ua/">
    <meta property="og:title" content="Создание сайта с `0` | Wordpress | Joomla | Landing | Laravel5">
    <meta property="og:title" content="Создание сайта с `0` | Wordpress | Joomla | Landing | Laravel5">
    <meta property="og:type" content="movie">
    <meta property="og:url" content="//optimist.biz.ua/">
    <meta property="og:image" content="//optimist.biz.ua/img/site.png">
    <meta property="og:site_name" content="Создание сайта с `0` | Wordpress | Joomla | Landing | Laravel5">
    <meta property="og:description" content="Профессиональное создание лэндинг пейдж, сайта компании,
        интернет магазина Верстка: landing page, шаблоны лэндингов +380669353050">

    <link rel="shortcut icon" href="#" type="image/x-icon">

    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css">

    <link rel="stylesheet" href="/front/assets/css/master.css">
</head>
<body>