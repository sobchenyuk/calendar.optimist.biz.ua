-- phpMyAdmin SQL Dump
-- version 3.4.10.1deb1
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Фев 21 2018 г., 09:39
-- Версия сервера: 5.6.34
-- Версия PHP: 5.3.10-1ubuntu3.26

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `calendar`
--

-- --------------------------------------------------------

--
-- Структура таблицы `calendar`
--

CREATE TABLE IF NOT EXISTS `calendar` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `slug` varchar(225) DEFAULT NULL,
  `event` varchar(225) DEFAULT NULL,
  `description` longtext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=54 ;

--
-- Дамп данных таблицы `calendar`
--

INSERT INTO `calendar` (`id`, `slug`, `event`, `description`) VALUES
(1, '29,0,2018', '234243', '234234'),
(2, '29,0,2018', '234243', '234234'),
(3, '29,0,2018', '234243', '234234'),
(4, '29,0,2018', '234243', '234234'),
(5, '29,0,2018', '234243', '234234'),
(6, '29,0,2018', '234243', '234234'),
(7, '29,0,2018', '234243', '234234'),
(8, '29,0,2018', '234243', '234234'),
(9, '29,0,2018', '234243', '234234'),
(10, '29,0,2018', 'fhf', 'fghfh'),
(11, '29,0,2018', 'fgh', 'fgh'),
(12, '30,0,2018', 'rty', 'rtytry'),
(13, '29,0,2018', 'dfg', 'dfgdfg'),
(14, '30,0,2018', 'tyutyu', 'tyutyu'),
(15, '31,0,2018', 'ghj', 'ghjgj'),
(16, '1,1,2018', 'dfg', 'dfgdfg'),
(17, '2,2,2018', 'fghf', 'fgh'),
(18, '25,1,2018', 'fghf', 'fhgfh'),
(19, '28,1,2018', 'fgh', 'fghf'),
(20, '27,1,2018', 'fgh', 'fghfgh'),
(21, '20,1,2018', 'fgh', 'fgh'),
(22, '14,1,2018', 'fghf', 'fgh'),
(23, '1,1,2018', 'ghjghj', 'ghj'),
(24, '3,1,2018', 'bnm', 'bnmb'),
(25, '4,1,2018', 'bnmb', 'bnm'),
(26, '20,1,2018', 'bnm', 'bnm'),
(27, '22,1,2018', 'bnm', 'bnm'),
(28, '1,2,2018', 'bmbnm', 'bnm'),
(29, '12,1,2018', 'bnm', 'bnm'),
(30, '30,0,2018', 'tyu', 'uytyu'),
(31, '5,1,2018', 'zxc', 'zxc'),
(32, '5,1,2018', 'fgh', 'fgh'),
(33, '1,1,2018', 'fg', 'fgh'),
(34, '29,0,2018', 'rty', 'rtyrt'),
(35, '29,0,2018', 'пропро', 'пропро'),
(36, '29,0,2018', 'dgdfg', 'dfgfdg'),
(37, '29,0,2018', 'dg', 'dfgdfg'),
(38, '29,0,2018', 'fgh', 'fgh'),
(39, '29,0,2018', 'dfgdfg', 'dfgdfg'),
(40, '29,0,2018', 'fgh1223', '123456'),
(41, '29,0,2018', 'ghj', 'ghjghj33'),
(42, '29,0,2018', 'tyutu', 'ty334535'),
(43, '29,0,2018', '43', '43ууууукк'),
(44, '30,0,2018', 'сми', 'смим'),
(45, '29,0,2018', 'dfg', 'dfgf'),
(46, '29,0,2018', 'fgfghf545', 'fghfghrtr5'),
(47, '30,0,2018', 'fghgh', 'fghfg'),
(48, '30,0,2018', 'df', 'dfgdfg'),
(49, '2,1,2018', 'fghfgh', 'fgh');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
