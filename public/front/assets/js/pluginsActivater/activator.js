$(document).ready(function () {
    $(window).scroll(function () {
        $(this).scrollTop() > 200 ? $("#toTop").fadeIn() : $("#toTop").fadeOut()
    }), $("#toTop").click(function () {
        close();
        return $("body,html").animate({scrollTop: 0}, 400), !1
    }), $("#toTopHover").hover(function () {
        $(this).animate({opacity: 1}, 500)
    }, function () {
        $(this).animate({opacity: 0}, 500)
    });

    $('.data-fancybox').fancybox({
        toolbar  : false,
    });

    let share = new Share({
        onShare: function(platform){
            //console.log(platform);
        }
    });

    $("#phone").inputmask({"mask": "(999) 999-9999"});

    $('.event-container').niceScroll({
        cursorcolor: "#4285f4",
        horizrailenabled: false,
    });
});