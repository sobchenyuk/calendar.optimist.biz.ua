<?php

define('PATCH', $_SERVER['DOCUMENT_ROOT']);

ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

$loader = require( __DIR__.'/../vendor/autoload.php' );
//$loader->addPsr4( 'App\\', __DIR__. './app/' );

use App\controllers\Controller;

$controller = new Controller();
$controller();
