<?php

namespace App\models;

use App\connect\Db;

class Calendar extends Db
{
    public $columns = "calendar";

    public $field = array(
        'id' => 'INT NOT NULL AUTO_INCREMENT',
        'slug' => 'VARCHAR(225) NULL',
        'event' => 'VARCHAR(225) NULL',
        'description' => 'LONGTEXT NULL'
    );

    public $primary = 'id';

}