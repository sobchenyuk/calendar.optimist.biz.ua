<?php

namespace App\controllers;

use App\models\Calendar;

class Controller
{
    protected $calendar;

    public function __construct()
    {
        $this->calendar = new Calendar();
    }

    public function __invoke()
    {

        switch ($_SERVER['REQUEST_URI']) {
            case ("/"):
                $this->view('index');
                break;
            case ("/select"):
                if ($_SERVER['REQUEST_METHOD'] == "POST") {
                        $a = 0;
                        $arr = array();
                        foreach ($this->calendar->select() as $rows) {
                            $arr[$a] = array(
                                'id' => $rows['id'],
                                'slug' => $rows['slug'],
                                'event' => $rows['event'],
                                'description' => $rows['description']
                            );
                            $a++;
                        }
                        echo json_encode($arr);
                } else {
                    $this->ErrorPage(404);
                }
                break;
            case ("/insert"):
                if ($_SERVER['REQUEST_METHOD'] == "POST"){
                    if($_POST['slug']){
                        $slug = trim(htmlspecialchars($_POST['slug']));
                        $event = trim(htmlspecialchars($_POST['event']));
                        $description = trim(htmlspecialchars($_POST['description']));

                        $field = array(
                            $slug, $event, $description
                        );

                        echo $this->calendar->insert($field);
                    }
                } else {
                    $this->ErrorPage(404);
                }
                break;
            case ("/update"):
                if ($_SERVER['REQUEST_METHOD'] == "POST"){
                    if($_POST['slug']){

                        $id = trim(htmlspecialchars($_POST['id']));
                        $event = trim(htmlspecialchars($_POST['event']));
                        $description = trim(htmlspecialchars($_POST['description']));

                        $field = array(
                            $event, $description, $id
                        );

                        if($this->calendar->update($field)){
                            echo 'Данные успешно обновлены';
                        } else {
                            echo 'Проблема с обновлением сервер';
                        }
                    }
                } else {
                    $this->ErrorPage(404);
                }
                break;
            case ("/delete"):
                if ($_SERVER['REQUEST_METHOD'] == "POST"){
                    if($_POST['slug']){

                        $id = trim(htmlspecialchars($_POST['id']));

                        if($this->calendar->delete($id)){
                            echo 'Данные успешно удалены';
                        } else {
                            echo 'Проблема с удалением сервер';
                        }
                    }
                } else {
                    $this->ErrorPage(404);
                }
                break;
            case ("/mail"):
                if ($_SERVER['REQUEST_METHOD'] == "POST"){
                    $this->email();
                } else {
                    $this->ErrorPage(404);
                }
                break;
            case ("/403"):
                    $this->ErrorPage(403);
                break;
            default:
                $this->ErrorPage(404);
        }

    }


    /**
     * @param $error
     * @return array
     */
    protected function setError($error){
        $arr = array();
        switch ($error) {
            case (404):
                $arr['title'] = 'Не найдена 404';
                $arr['h1'] = 'Страница не найдена 404';
                $arr['p'] = 'Извините, но страница, которую вы пытались просмотреть, не существует.';
                break;
            case (403):
                $arr['title'] = 'Нет прав 403';
                $arr['h1'] = 'У вас нет прав для просмотра данной страницы 403';
                $arr['p'] = 'Извините, но страница, которую вы пытались просмотреть, не доступна для просмотра.';
                break;
        }

        return $arr;
    }

    /**
     * @param $error
     */
    public function ErrorPage($error)
    {
        header('HTTP/1.1 '.$error.' Not Found');
        header('Status: '.$error.' Not Found');
        $this->view('error/error', $this->setError($error));
        exit;
    }

    /**
     * @param $template
     * @param null $arr
     * @return bool
     */
    public function view($template, $arr = null)
    {
        require( PATCH .'/view/'.$template.'.php');

        return false;
    }

    /**
     * email
     */
    protected function email()
    {
        if ($_POST['name']) {
            $name = trim(htmlspecialchars($_POST['name']));
            $phone = trim(htmlspecialchars($_POST['phone']));
            $email = trim(htmlspecialchars($_POST['email']));
            $description = trim(htmlspecialchars($_POST['description']));


            $to = 'ablehands@ukr.net';
            $subject = 'calendar.optimist.biz.ua';

            $headers = 'MIME-Version: 1.0' . "\r\n";
            $headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
            $headers .= 'From: ' . $email . ' ' . "\r\n";

            $message = $this->template($name, $phone, $email, $description);

            mail($to, $subject, $message, $headers);

            $headersClient = 'MIME-Version: 1.0' . "\r\n";
            $headersClient .= 'Content-type: text/html; charset=utf-8' . "\r\n";
            $headersClient .= 'From: ' . $to . ' ' . "\r\n";

            $messageClient = "Спасибо за сообщение.";

            mail($email, $subject, $messageClient, $headersClient);

            echo 'Сообщение успешно отправлено';

            die();

        }
    }


    /**
     * @param $name
     * @param $phone
     * @param $email
     * @param $description
     * @return string
     */
    protected function template($name, $phone, $email, $description)
    {
        return "
        <html>
<head>
    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />
    <title>Письмо с сайта &nbsp;&nbsp;calendar.optimist.biz.ua</title>
</head>
<body>

<h2>
    Письмо с сайта &nbsp;&nbsp;calendar.optimist.biz.ua
</h2>

<table style=\"background-color:white;
       -webkit-border-radius:15px; -moz-border-radius:15px;
        border-radius:15px; box-shadow: 0 0 5px black;
         box-shadow: 0 0 10px rgba(0,0,0,0.5); -moz-box-shadow: 0 0 10px rgba(0,0,0,0.5);
          -webkit-box-shadow: 0 0 10px rgba(0,0,0,0.5); padding:20px; font-size:1em\">
    <tr><td>От кого: {$name}</td></tr>
    <tr><td>Телефон: {$phone}</td></tr>
    <tr><td>Адрес: {$email}</td></tr>
    <tr><td>Текст сообщения: {$description}</td></tr>
</table>

</body>
</html>";
    }
}