<?php

namespace App\connect;


class Db implements iTemplate
{
    private  $_db = null;

    /**
     * $columns имя таблицы базы данных
     */
    public $columns;

    /**
     * $field массив с полями из таблицы базы данных
     */
    public $field;

    /**
     * $primary уникальный ключ таблицы
     */
    public $primary;
    /**
     * Db constructor соединение с базой данных.
     */
    function __construct()
    {
        $this->_db = ConnectDB::getInstance();
    }

    /**
     * @return mixed выбор из базы данных
     */
    public function select()
    {
        return $this->_db->query("SELECT * FROM $this->columns WHERE 1");
    }

    /**
     * @param $field
     * добавление в базу
     * @return int
     */
    public function insert($field)
    {
        $sql = $this->_db->prepare('INSERT INTO `' . $this->columns . '` (`slug`,`event`,`description`) VALUES (:slug, :event, :description)');
        $data = array('slug' => $field[0], 'event' => $field[1], 'description' => $field[2]);
        $sql->execute($data);
        return $this->_db->lastInsertId();
    }

    /**
     * param $field
     * @return mixed обновление данных
     */
    public function update($field)
    {
        $sql = $this->_db->prepare("UPDATE `$this->columns` SET `description` = :description, `event` = :event WHERE `id` = :id LIMIT 1");
        return $sql->execute(array(
            ':id' => $field[2],
            ':event' => $field[0],
            ':description' => $field[1]
        ));
    }

    /**
     * @param $id
     * @return mixed удаление данных
     */
    public function delete($id)
    {
        $sql = 'DELETE FROM `'. $this->columns .'` WHERE `id` = :id LIMIT 1';
        $query = $this->_db->prepare($sql);
        return $query->execute(array(":id" => $id));
    }


    /**
     * example $calendar->create_table();
     */
    public function create_table(){

        $table = $this->_db->query("SELECT * FROM $this->columns WHERE 1");

        if(!$table){
                    $sql = "CREATE TABLE `". $this->columns ."`  ( ";
        foreach ($this->field as $value => $type)
            $sql .= '`' . $value . '` ' . $type .', ';
        $sql .="PRIMARY KEY (`$this->primary`))";
            if (!$this->_db->exec($sql)) {
                echo "Таблица $this->columns создана успешно";
            } else {
                echo "Ошибка создания таблицы: ";
            }

        } else {
            echo 'Таблица с таким именем уже существует';
        }
    }

    /**
     * example $calendar->drop_table();
     */
    public function drop_table(){

        $table = $this->_db->query("SELECT * FROM $this->columns WHERE 1");

        if(!$table){
            echo 'Таблица с таким именем не существует';
        } else {
            $sql = 'DROP TABLE `'. $this->columns .'` ';
            if (!$this->_db->exec($sql)) {
                echo "Таблица $this->columns успешно удалена";
            } else {
                echo "Ошибка удаления таблицы: ";
            }
        }

    }

}