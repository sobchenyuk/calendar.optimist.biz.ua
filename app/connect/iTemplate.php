<?php

namespace App\connect;


interface iTemplate
{
    /**
     * @return mixed выбор из базы данных
     */
    public function select();


    /**
     * @param $field
     * @return mixed добавление в базу
     */
    public function insert($field);

    /**
     * @param $field
     * @return mixed обновление данных
     */
    public function update($field);

    /**
     * @param $id
     * @return mixed удаление данных
     */
    public function delete($id);

    /**
     * @return mixed создание таблицы
     * example $calendar->create_table();
     */
    public function create_table();


    /**
     * Удаление таблицы
     * example $calendar->drop_table();
     */
    public function drop_table();
}