<?php

namespace App\connect;

use PDO;

class ConnectDB
{
    private static $_db = null;

    private static $db_host = '127.0.0.1';
    private static $db_name = 'calendar';
    private static $db_user = 'calendar';
    private static $db_pass = 'yUpyuzUE';

    private function __construct(){}

    private function __clone(){}

    private function __wakeup(){}

    public static function getInstance()
    {
        if (self::$_db === null) {
            self::$_db = new PDO('mysql:host='.self::$db_host.';dbname='.
                self::$db_name.';charset=UTF8', self::$db_user, self::$db_pass);
            self::$_db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }

        return self::$_db;
    }
}
